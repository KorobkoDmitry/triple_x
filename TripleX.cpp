#include <iostream>
#include <ctime>

int RandomByDifficulty(int Difficulty)
{
    return rand() % Difficulty + 1;
}

void PrintIntroduction(int Difficulty)
{
    // Print welcome messages to the terminal
    std::cout << "\nYou are a professional thief who has encroached on an ancient precious relic of the " << Difficulty;
    std::cout << " level of difficulty, but the door to it is closed with a multi-step security code.\n";
    std::cout << "To open the door without raising an alarm, you need to crack code...\n\n";
}

bool PlayGame(int Difficulty)
{ 
    PrintIntroduction(Difficulty);

    // Declare 3 number code 
    const int CodeA = RandomByDifficulty(Difficulty);
    const int CodeB = RandomByDifficulty(Difficulty);
    const int CodeC = RandomByDifficulty(Difficulty);

    const int CodeSum = CodeA + CodeB + CodeC;
    const int CodeProduct = CodeA * CodeB * CodeC;

    // Print CodeSum and CodeProduct to the terminal
    std::cout << "+ There are 3 numbers in the code";
    std::cout << "\n+ The codes add up to: " << CodeSum;
    std::cout << "\n+ The codes multiply to give: " << CodeProduct << std::endl;

    // Store player guess
    int GuessA, GuessB, GuessC;

    std::cin >> GuessA >> GuessB >> GuessC;

    int GuessSum = GuessA + GuessB + GuessC;
    int GuessProduct = GuessA * GuessB * GuessC;

    // Check if the players guess is correct
    if (GuessSum == CodeSum && GuessProduct == CodeProduct)
    {
        std::cout << "\nCool! This code is correct! Level completed!";

        return true;
    }
    else
    {
        std::cout << "\nBad news for you - code is wrong! Try again!";

        return false;
    }
}

int main()
{ 
    srand(time(NULL)); // Create new random sequence based on time of day

    const int MaxDifficulty = 3;

    int LevelDifficulty = 1;

    while (LevelDifficulty <= MaxDifficulty) // Loop game until all levels completed
    {
        bool bLevelComplete = PlayGame(LevelDifficulty);

        std::cin.clear(); // Clears any errors
        std::cin.ignore(); // Discards the buffer

        if (bLevelComplete)
        {
            ++LevelDifficulty;
        }
        
    }

    std::cout << "\n --------------------------------------------";
    std::cout << "\n| Congratulations! You totally win the game! |";
    std::cout << "\n --------------------------------------------";

    return 0;
}